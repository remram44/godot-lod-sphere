extends Spatial

const CAMERA_ROTATE_SPEED = 0.001
const MOVE_SPEED = 0.2

func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)

func _input(event):
	if event is InputEventMouseMotion:
		# Pitch up/down
		var rx = $Camera.rotation.x
		var trx = rx - event.relative.y * CAMERA_ROTATE_SPEED
		trx = clamp(trx, -0.5 * PI, 0.5 * PI)
		$Camera.rotate_x(trx - rx)

		# Yaw left/right
		rotate_object_local(Vector3(0, 1, 0), -CAMERA_ROTATE_SPEED * event.relative.x)

func _process(delta):
	# Exit
	if Input.is_action_pressed("ui_cancel"):
		get_tree().quit()

	# Move
	var move = Vector3()
	move.z += Input.get_action_strength("p1_back")
	move.z -= Input.get_action_strength("p1_forward")
	move.x += Input.get_action_strength("p1_right")
	move.x -= Input.get_action_strength("p1_left")
	move.y += Input.get_action_strength("p1_up")
	move.y -= Input.get_action_strength("p1_down")
	translate($Camera.transform.basis.xform(move * MOVE_SPEED))

	# Keep bottom towards center
	var our_y = transform.basis.xform(Vector3(0, 1, 0))
	var wanted_y = (translation - Vector3(0, 0, 0)).normalized()
	var rot = our_y.cross(wanted_y)
	if rot.length_squared() >= 0.00001:
		rotate(rot.normalized(), rot.length() * 0.9)

	# Stay outside of sphere
	var sqdist = translation.length_squared()
	if sqdist < 102.0 * 102.0:
		translation *= 102.0 / sqrt(sqdist)
