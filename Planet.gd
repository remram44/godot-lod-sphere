extends Spatial

onready var faces = [$FaceUp, $FaceDown, $FaceLeft, $FaceRight, $FaceFront, $FaceBack]

func _ready():
	for face in faces:
		face.level = 0
		face.update_face()
