extends Spatial

const RADIUS = 100.0
const MIN_LEVEL = 1
const MAX_LEVEL = 7

const material = preload("res://face.material")
onready var PlanetFace = load("res://PlanetFace.tscn")

# Set by parent before he calls update_face()
var level
var x1 = -1.0
var x2 = 1.0
var z1 = -1.0
var z2 = 1.0

var mesh_instance: MeshInstance = null
var children_faces = []
var counter = randi() % 60
var our_origin
var threshold

func add_point(surface_tool, color, x, z):
	var coords = Vector3(x, 1, z)

	# Project on the sphere
	coords = coords.normalized() * RADIUS

	# Add point
	surface_tool.add_color(color)
	surface_tool.add_vertex(coords)

func clear_faces():
	if mesh_instance:
		mesh_instance.queue_free()
		mesh_instance = null
	if children_faces:
		for chf in children_faces:
			chf.queue_free()
		children_faces = []

func make_mesh():
	clear_faces()

	var surface_tool = SurfaceTool.new()
	var color
	surface_tool.begin(Mesh.PRIMITIVE_TRIANGLES)
	color = Color(1, 0.4, 0.4, 1)
	add_point(surface_tool, color, x1, z2)
	add_point(surface_tool, color, x1, z1)
	add_point(surface_tool, color, x2, z2)

	color = Color(0.4, 0.4, 1, 1)
	add_point(surface_tool, color, x2, z2)
	add_point(surface_tool, color, x1, z1)
	add_point(surface_tool, color, x2, z1)

	surface_tool.generate_normals()
	mesh_instance = MeshInstance.new()
	mesh_instance.mesh = surface_tool.commit()
	mesh_instance.mesh.surface_set_material(0, material)
	add_child(mesh_instance)

func make_children():
	clear_faces()

	var face

	face = PlanetFace.instance()
	face.level = level + 1
	face.x1 = x1
	face.x2 = (x1 + x2) / 2
	face.z1 = z1
	face.z2 = (z1 + z2) / 2
	children_faces.append(face)
	add_child(face)
	face.update_face()

	face = PlanetFace.instance()
	face.level = level + 1
	face.x1 = (x1 + x2) / 2
	face.x2 = x2
	face.z1 = z1
	face.z2 = (z1 + z2) / 2
	children_faces.append(face)
	add_child(face)
	face.update_face()

	face = PlanetFace.instance()
	face.level = level + 1
	face.x1 = x1
	face.x2 = (x1 + x2) / 2
	face.z1 = (z1 + z2) / 2
	face.z2 = z2
	children_faces.append(face)
	add_child(face)
	face.update_face()

	face = PlanetFace.instance()
	face.level = level + 1
	face.x1 = (x1 + x2) / 2
	face.x2 = x2
	face.z1 = (z1 + z2) / 2
	face.z2 = z2
	children_faces.append(face)
	add_child(face)
	face.update_face()

func _process(delta):
	counter = (counter + 1) % 60
	if counter != 0:
		return

	update_face()

func update_face():
	if our_origin == null:
		our_origin = Vector3((x1 + x2) / 2, 1, (z1 + z2) / 2)
		our_origin = our_origin.normalized() * RADIUS
		our_origin = global_transform.xform(our_origin)
	if threshold == null:
		threshold = (x2 - x1) * (x2 - x1) * RADIUS * RADIUS * 1.0

	# Find closest viewer, to decide level of details
	var closest_viewer_sqdist = INF
	for viewer in get_tree().get_nodes_in_group("viewers"):
		var viewer_sqdist = (viewer.global_transform.origin - our_origin).length_squared()
		if closest_viewer_sqdist == null or viewer_sqdist < closest_viewer_sqdist:
			closest_viewer_sqdist = viewer_sqdist

	if level >= MIN_LEVEL and (closest_viewer_sqdist > threshold or level >= MAX_LEVEL):
		if not mesh_instance:
			make_mesh()
	else:
		if not children_faces:
			make_children()
